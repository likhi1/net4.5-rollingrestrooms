﻿
function DialogFloat( isOpen ,  isModal,  dialogContentId, dialogButtonId, widthSize, heightSize, positionX, positionY  , addClass ) {
    //========  Jquery UI-Dialog  LiveChat
    jQuery(dialogContentId).dialog({
        show: "fade",
        hide: "fade",
        autoOpen: isOpen,
        dialogClass: addClass , // Fix position to bottom
        modal: isModal,
        width: widthSize,
        height : heightSize ,  
        rsesizable: false,    // option   
        position: { my: positionX, at: positionY , of: window },
        //position: { my: "right  top", at: "right   bottom", of: window },
        open: function () { // bind close when click overlay 
            jQuery(dialogContentId + " .buttonDialogClose").bind('click', function () {
                jQuery(dialogContentId).dialog('close');
            })
        }  

        //dialogClass: 'yourclassname', 
        //draggable: false 

    });

    /////// Config UI
    $(".ui-dialog-titlebar-close" ).attr("style", "top:50%") 
    $(".ui-dialog-titlebar").hide();  
   


    //$(".ui-dialog").css("z-index", "11111");
    //$("#dialogBannerFloat .bx-wrapper").attr("style", "float:left; ");  
    //$(".ui-corner-all").css("z-index", "11111");
    //$(".ui-widget-overlay").css({ "z-index": 11111, "opacity": 0.7, "background-image": none, "background-color": "#131313" });
    //$(".ui-widget-content").css({ "border": none });


    ////// command event  dialog
    $(dialogButtonId).click(function (e) {
        //console.log(dialogButtonId+" Clicked");
        e.preventDefault();
        $(dialogContentId).dialog('open');
    })

}
 