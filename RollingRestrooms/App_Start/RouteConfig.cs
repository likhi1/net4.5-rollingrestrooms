﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RollingRestrooms {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            ///////// กรณีเป็นชื่อ Action  พิมพ์มา
            routes.MapRoute(
            "OnlyAction",
            "{action}",  // หมายถึง กรณี Url ใส่แค่ชื่อ Action ให้เรียกใช้ Controlller = “home” 
            new { controller = "Home", action = "index" }  // ธรรมดาให้วิ่งเข้า  Controlller = “home”   และเข้า  Action = “About”   
            ); 

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}