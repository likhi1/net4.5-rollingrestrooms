﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RollingRestrooms.Models.ViewModel
{
    public class ad_userinfoVM
    {
        public string sad_id { get; set; }
        public string ad_id { get; set; }
      [Display(Name="Domain desc")]
        [Required]
      
        public string ad_name { get; set; }
        public string SGID { get; set; }
        public string user_name { get; set; }
        [Display(Name = "User Email")]
        [Required]
        public string user_email { get; set; }
        
        public Nullable<System.DateTime> insert_datetime { get; set; }
        public Nullable<System.DateTime> inactive_datetime { get; set; }
        public Nullable<System.DateTime> sync_datetime { get; set; }
        


    }
}