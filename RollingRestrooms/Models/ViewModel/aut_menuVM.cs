﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RollingRestrooms.Models.ViewModel
{
    public class aut_menuVM
    {
        public string menu_id { get; set; }
        public string modu_id { get; set; }
        public int menu_no { get; set; }
        public string menu_name { get; set; }
        public string menu_filepath { get; set; }
        public string menu_status { get; set; }
        public Nullable<System.DateTime> menu_canceldate { get; set; }
        public string menu_cancellogin { get; set; }
        public System.DateTime insert_date { get; set; }
        public string insert_login { get; set; }
        public Nullable<System.DateTime> update_date { get; set; }
        public string update_login { get; set; }
    }
}