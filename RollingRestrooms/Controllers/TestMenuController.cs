﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RollingRestrooms.Models;

namespace RollingRestrooms.Controllers
{
    public class TestMenuController : Controller
    {
        private UserManagementEntities db = new UserManagementEntities();

        //
        // GET: /TestMenu/

        public ActionResult Index()
        {
            var model = db.aut_menu
                .OrderBy(x => x.menu_id).ThenBy(x => x.menu_no)
                .Where(x => x.menu_no >= 2 && x.menu_no <=4 )
                .ToList();

           // ViewBag.test = "jjjjjjjjjjj";

            return View(model);

            //return View(db.aut_menu.ToList());
        }

        //
        // GET: /TestMenu/Details/5

        public ActionResult Details(string id = null)
        {
            aut_menu aut_menu = db.aut_menu.Find(id);
            if (aut_menu == null)
            {
                return HttpNotFound();
            }
            return View(aut_menu);
        }

        //
        // GET: /TestMenu/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TestMenu/Create

        [HttpPost]
        public ActionResult Create(aut_menu aut_menu)
        {
            if (ModelState.IsValid)
            {
                db.aut_menu.Add(aut_menu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aut_menu);
        }

        //
        // GET: /TestMenu/Edit/5

        public ActionResult Edit(string id = null)
        {
            aut_menu aut_menu = db.aut_menu.Find(id);
            if (aut_menu == null)
            {
                return HttpNotFound();
            }
            return View(aut_menu);
        }

        //
        // POST: /TestMenu/Edit/5

        [HttpPost]
        public ActionResult Edit(aut_menu aut_menu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aut_menu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aut_menu);
        }

        //
        // GET: /TestMenu/Delete/5

        public ActionResult Delete(string id = null)
        {
            aut_menu aut_menu = db.aut_menu.Find(id);
            if (aut_menu == null)
            {
                return HttpNotFound();
            }
            return View(aut_menu);
        }

        //
        // POST: /TestMenu/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            aut_menu aut_menu = db.aut_menu.Find(id);
            db.aut_menu.Remove(aut_menu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}